function load_assets()
  local mag = {
    bod = love.graphics.newImage("res/mag_bod.png"),
    arm_b = love.graphics.newImage("res/mag_arm_b.png"),
    arm_f = love.graphics.newImage("res/mag_arm_f.png"),
    leg = love.graphics.newImage("res/mag_leg.png"),
  }
  local ulf = {
    bod = love.graphics.newImage("res/ulf_bod.png"),
    arm_b = love.graphics.newImage("res/ulf_arm_b.png"),
    arm_f = love.graphics.newImage("res/ulf_arm_f.png"),
    leg = love.graphics.newImage("res/ulf_leg.png"),
  }
  assets = {
    mag = mag,
    ulf = ulf,
  }
end

function draw_ledare(ledare, player, moving)
  if player.is_moving then
    draw_moving_player(ledare, player)
  else
    draw_standing_player(ledare, player)
  end
end

function draw_moving_player(ledare, player)
  local sx = 1
  if player.invert_img then
    sx = -1
  end
  love.graphics.setColor(1.0, 1.0, 1.0)
  -- draw left leg
  -- draw right leg
  love.graphics.draw(ledare.bod, player.x, player.y)
end

function draw_standing_player(ledare, player)
  local sx = 1
  if player.invert_img then
    sx = -1
  end
  love.graphics.setColor(1.0, 1.0, 1.0)
  love.graphics.draw(ledare.leg, player.x +20, player.y +63)
  love.graphics.draw(ledare.leg, player.x +10, player.y +63)
  love.graphics.draw(ledare.arm_b, player.x -5, player.y +33)
  love.graphics.draw(ledare.bod, player.x, player.y)
  love.graphics.draw(ledare.arm_f, player.x +10, player.y +33)
  love.graphics.draw(ledare.bod, player.x, player.y, 0, sx, 1, 13)
end
