players = 
{
  {
    x = 0.0,
    y = 0.0,
    invert_img = false,
    is_moving = false,
    animation = 1
  },
}

function update_main_menu(delta, controllers)
end

function draw_main_menu()
end

function update_character_select(delta, controllers)
end

function draw_character_select()
end

function update_playing(delta, controllers)
  players[1].x = players[1].x + controllers[1].axis.left.x * delta * 100.0
  players[1].y = players[1].y + controllers[1].axis.left.y * delta * 100.0
  if controllers[1].axis.left.x > 0 then
    players[1].invert_img = true
  elseif controllers[1].axis.left.x < 0 then
    players[1].invert_img = false
  else
    -- NOTE: == 0, do nothing
  end
end

function draw_playing()
  draw_ledare(assets.mag, players[1])
end

function update_game_end(delta, controllers)
end

function draw_game_end()
end
