require("state")
require("input")
require("asset")

function love.load()
  load_assets()
  love.window.setMode(1440, 810, { resizable = false })
  state = "playing"
end

function love.update(delta)
  local controllers = {}
  for i = 1,getNumControllers() do
    table.insert(controllers, getInput(i))
  end

  if state == "main menu" then
    update_main_menu(delta, controllers)
  elseif state == "character select" then
    update_character_select(delta, controllers)
  elseif state == "game end" then
    update_game_end(delta, controllers)
  else
    update_playing(delta, controllers)
  end
end

function love.draw()
  if state == "main menu" then
    draw_main_menu()
  elseif state == "character select" then
    draw_character_select()
  elseif state == "game end" then
    draw_game_end()
  else
    draw_playing()
  end
end
