function printJoyStick()
  local joysticks = love.joystick.getJoysticks()
  for j, joystick in ipairs(joysticks) do
    print("----------------------------")
    print(j, joystick:getName())
    axisLeftX, axisLeftY, axisLeftTrig, axisRightX, axisRightY, axisRightTrig = joystick:getAxes()
    print(axisLeftX, axisLeftY, axisLeftTrig, axisRightX, axisRightY, axisRightTrig)
  end
end

function getInput(numPlayers)
  local joystick = love.joystick.getJoysticks()[numPlayers]
  axisLeftX, axisLeftY, axisLeftTrig, axisRightX, axisRightY, axisRightTrig = joystick:getAxes()
  if math.abs(axisLeftX) < 0.15 then
    axisLeftX = 0.0
  end
  if math.abs(axisLeftY) < 0.15 then
    axisLeftY = 0.0
  end
  return {
    axis = {
      left = {
        x = axisLeftX,
        y = axisLeftY,
      },
      right = {
        x = axisRightX,
        y = axisRightY,
      },
    },
    trig = {
      left = axisLeftTrig,
      right = axisRightTrig,
    }
  }
end

function getNumControllers()
  return table.getn(love.joystick.getJoysticks())
end
